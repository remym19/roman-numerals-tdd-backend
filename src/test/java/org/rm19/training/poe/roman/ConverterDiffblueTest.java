package org.rm19.training.poe.roman;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ConverterDiffblueTest {

    @Test
    void toRoman_plus1() {
        // Arrange
        Converter converter = new Converter();
        int positive = 1;

        // Act
        String actualToRomanResult = converter.toRoman(positive);

        // Assert
        assertEquals("I", actualToRomanResult);
    }

    @Test
    void toRoman_0() {
        // Arrange
        Converter converter = new Converter();
        int zero = 0;

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> converter.toRoman(zero));
    }

    @Test
    void toRoman_minus1() {
        // Arrange
        Converter converter = new Converter();
        int negative = -1;

        // Act
        String actualToRomanResult = converter.toRoman(negative);

        // Assert
        assertEquals("-I", actualToRomanResult);
    }
}

