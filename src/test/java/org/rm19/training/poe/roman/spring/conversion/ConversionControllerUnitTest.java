package org.rm19.training.poe.roman.spring.conversion;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ConversionControllerUnitTest {

    @Test
    void arabicToRoman_shouldConvert_1() {
        shouldConvertArabicToRoman(1, "I");
    }

    @Test
    void arabicToRoman_shouldConvert_43() {
        shouldConvertArabicToRoman(43, "XLIII");
    }

    @ParameterizedTest(name = "arabic {0} to roman {1}")
    @CsvFileSource(resources = "/arabic-roman_up-to-50.csv", numLinesToSkip = 1)
    void shouldConvertArabicToRoman_fromCsv_upTo50(int arabic, String expectedRoman) {
        shouldConvertArabicToRoman(arabic, expectedRoman);
    }

    @ParameterizedTest(name = "arabic {0} to roman {1}")
    @CsvFileSource(resources = "/arabic-roman_big-sample.csv", numLinesToSkip = 1)
    void shouldConvertArabicToRoman_fromCsv_bigSample(int arabic, String expectedRoman) {
        shouldConvertArabicToRoman(arabic, expectedRoman);
    }

    private void shouldConvertArabicToRoman(int arabic, String expectedRoman) {
        ConversionController controller = new ConversionController();

        String actualRoman = controller.arabicToRoman(arabic);

        assertEquals(expectedRoman, actualRoman);
    }

}