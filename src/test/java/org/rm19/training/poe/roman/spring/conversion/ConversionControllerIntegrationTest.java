package org.rm19.training.poe.roman.spring.conversion;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class ConversionControllerIntegrationTest {

    @LocalServerPort private int port;

    @Test
    void arabicToRoman_shouldBeAPostEndpoint() {
        shouldConvertArabicToRoman(1, "I");
    }

    @ParameterizedTest(name = "arabic {0} to roman {1}")
    @CsvFileSource(resources = "/arabic-roman_up-to-50.csv", numLinesToSkip = 1)
    void shouldConvertArabicToRoman_fromCsv_upTo50(int arabic, String expectedRoman) {
        shouldConvertArabicToRoman(arabic, expectedRoman);
    }

    @ParameterizedTest(name = "arabic {0} to roman {1}")
    @CsvFileSource(resources = "/arabic-roman_big-sample.csv", numLinesToSkip = 1)
    void shouldConvertArabicToRoman_fromCsv_bigSample(int arabic, String expectedRoman) {
        shouldConvertArabicToRoman(arabic, expectedRoman);
    }

    private void shouldConvertArabicToRoman(int arabic, String expectedRoman) {
        // Initialize
        TestRestTemplate rest = new TestRestTemplate("", "");
        RequestEntity<?> request = RequestEntity.post("http://localhost:" + port + "/converter").body(arabic);

        // Execute
        ResponseEntity<String> response = rest.exchange(request, String.class);

        // Verify
        assertEquals(HttpStatus.OK, response.getStatusCode());
        String body = response.getBody();
        assertEquals(expectedRoman, body);
    }

}