package org.rm19.training.poe.roman;

import lombok.AllArgsConstructor;

import java.util.List;

import static java.util.Arrays.asList;

@AllArgsConstructor
public class Mapping {

    public static final List<Mapping> MAPPINGS = asList(
            new Mapping(1_000_000, "(M)"),
            new Mapping(500_000, "(D)"),
            new Mapping(100_000, "(C)"),
            new Mapping(50_000, "(L)"),
            new Mapping(10_000, "(X)"),
            new Mapping(5_000, "(V)"),
            new Mapping(1_000, "M"),
            new Mapping(900, "CM"),
            new Mapping(500, "D"),
            new Mapping(400, "CD"),
            new Mapping(100, "C"),
            new Mapping(90, "XC"),
            new Mapping(50, "L"),
            new Mapping(40, "XL"),
            new Mapping(10, "X"),
            new Mapping(9, "IX"),
            new Mapping(5, "V"),
            new Mapping(4, "IV"),
            new Mapping(1, "I")
    );

    // wayup: make public and final
    int arabic;
    String roman;

}
