package org.rm19.training.poe.roman.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RomanNumeralsSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(RomanNumeralsSpringApplication.class, args);
	}

}
