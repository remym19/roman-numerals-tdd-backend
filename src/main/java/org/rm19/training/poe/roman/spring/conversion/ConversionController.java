package org.rm19.training.poe.roman.spring.conversion;

import org.rm19.training.poe.roman.Converter;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/converter")
public class ConversionController {

    private final Converter converter = new Converter();

    @PostMapping
    @CrossOrigin(origins = "localhost")
    public String arabicToRoman(@RequestBody int arabic) {
        return converter.toRoman(arabic);
    }
}
